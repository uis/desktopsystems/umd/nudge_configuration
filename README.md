## Nudge JSON Configuration

## Description
A collection of configurations for the NUdge client to pull down and impliment. Each configuration specifies a different target version of macOS and a different end-goal version of macOS. Each configuration will be scoped in Jamf to the correct collection of devices.

## nudge12cap12to12.json
This configuration targets macOS 12 machines and updates to the latest version of macOS 12. Used for machines not capable of upgrading beyond macOS 12

## nudge13cap12to12.json
THis configuration targets macOS 12 machines that ARE capable of upgrading to macOS 13 but don't want to change macOS version yet (due to lingering issues with existing software)

## nudge13cap13to13.json
This configuration targets macOS13 machines and updates them to the latest version of macOS 13 that is available.

## Usage
There are 3 settings that require changing when an update is released or a new deadline is needed for devices

- "requiredInstallationDate": "2023-04-27T00:00:00Z"
- "requiredMinimumOSVersion": "12.6.5"
- "targetedOSVersionsRule": "12"

Adjust each setting as required
- requiredInstallationDate of format YYYY-MM-DDThh:mm:ssZ
- requiredMinimumOSVersion: the version of macOS that you require the device to end up on
- targetedOSVersionsRule: the version of macOS that will be targetted by this configuration. Can be just the major version (e.g. 12) or with minor updates (e.g. 12.4, 12.6.5)

## Other Useful Information
- "majorUpgradeAppPath" is used on particular update configurations to allow a delta update to happen rather than a full update download
